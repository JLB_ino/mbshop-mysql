<%@ page import="mbshop.model.Item" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>テッド - エム・ビー ショップ</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<h1>エム・ビー ショップ</h1>
<h2>テッド</h2>
<div class="image"><img src="img/${item.id }.jpg" width="180"></div>
<div class="detail">
    <dl>
        <dt>タイトル</dt>
        <dd>${item.title }</dd>
        <dt>価格</dt>
        <dd>${item.price }円</dd>
        <dt>出演</dt>
        <dd>${item.players }</dd>
        <dt>監督</dt>
        <dd>${item.directors }</dd>
        <dt>解説</dt>
        <dd>${item.description }</dd>
    </dl>
    <p><a href="CartController?mode=1&id=${item.id }"><button type="button">カートに入れる</button></a></p>
</div>
<p><a href="ItemsController">&lt;&lt; トップページへ</a></p>
</body>
</html>