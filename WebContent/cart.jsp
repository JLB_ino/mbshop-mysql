<%@ page import="mbshop.model.Item" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート - エム・ビー ショップ</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<h1>エム・ビー ショップ</h1>
<h2>カート</h2>
<table class="cart">
    <tr>
        <th>タイトル</th>
        <th>価格</th>
        <th>数量</th>
        <th>計</th>
        <th>削除</th>
    </tr>
    <c:set var="total" value="0"/>
    <c:forEach var="cartEntry" items="${cart}">
        <c:set var="item" value="${cartEntry.value.item }"/>
        <c:set var="quantity" value="${cartEntry.value.quantity }"/>
	    <tr>
	        <td>${item.title }</td>
	        <td>${item.price }円</td>
	        <td>
	           <form action="CartController" method="post">
                    <input type="hidden" name="mode" value="2">
	                <input type="hidden" name="id" value="${item.id }">
	                <input type="text" name="quantity" value="${quantity }" size="3">
	                <button type="submit">更新</button>
	           </form>
	           </td>
	        <td>${item.price * quantity }円</td>
	        <td><a href="CartController?mode=3&id=${item.id }"><button type="button">削除</button></a></td>
	    </tr>
	    <c:set var="total" value="${total + item.price * quantity }"/>
    </c:forEach>
    <tr>
        <th colspan="3">合計</th>
        <th>${total }円</th>
        <th> </th>
    </tr>
</table>
<p class="center">
   <a href="ItemsController"><button type="button">買い物を続ける</button></a>
</p>
<h2>お届け先</h2>
<form class="address" action="OrdersController?mode=1" method="post">
	<dl>
    	<dt>名前</dt>
    	<dd><input type="text" name="name" value="" size="20"></dd>
        <dt>郵便番号</dt>
        <dd><input type="text" name="postal" value="" size="8"></dd>
    	<dt>住所</dt>
    	<dd><input type="text" name="address" value="" size="40"></dd>
    	<dt>電話番号</dt>
    	<dd><input type="text" name="phone" value="" size="20"></dd>
	</dl>
    <p><a href=""><button type="submit">注文する</button></a></p>
</form>
</body>
</html>