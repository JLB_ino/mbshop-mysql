<%@ page import="java.util.List" %>
<%@ page import="mbshop.model.Item" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品一覧 - エム・ビー ショップ</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<h1>エム・ビー ショップ</h1>
<h2>商品一覧</h2>
<form action="ItemsController" method="get">
	<input type="text" name="keyword" value="${keyword }">
	<button type="submit">検索</button>
</form>
<div class="page-navi">
    <c:if test="${page > 1 }">
        <a href="ItemsController?keyword=${keyword }&page=${page - 1 }">&lt;&lt; 前</a>
    </c:if>
    <c:forEach var="i" begin="1" end="${numrows / ITEMS_OF_PAGE + (numrows % ITEMS_OF_PAGE > 0 ? 1 : 0) }">
        <c:choose>
            <c:when test="${i != page }">
                <a href="ItemsController?keyword=${keyword }&page=${i }">${i }</a>
            </c:when>
            <c:otherwise>${i }</c:otherwise>
        </c:choose>
    </c:forEach>
    <c:if test="${(page * ITEMS_OF_PAGE) < numrows }">
        <a href="ItemsController?keyword=${keyword }&page=${page + 1 }">次 &gt;&gt;</a>
    </c:if>
</div>
<c:forEach var="item" items="${list}">
<div class="item">
    <div class="image"><img src="img/${item.id}.jpg" width="100"></div>
    <dl class="description">
        <dt>タイトル</dt>
        <dd><a href="ItemsController?mode=1&id=${item.id}">${item.title}</a></dd>
        <dt>価格</dt>
        <dd>${item.price}円</dd>
    </dl>
    <p class="button"><a href="/mbshop/CartController?mode=1&id=${item.id}"><button type="button">カートに入れる</button></a></p>
</div>
</c:forEach>
<div class="page-navi">
    <c:if test="${page > 1 }">
        <a href="ItemsController?keyword=${keyword }&page=${page - 1 }">&lt;&lt; 前</a>
    </c:if>
    <c:forEach var="i" begin="1" end="${numrows / ITEMS_OF_PAGE + (numrows % ITEMS_OF_PAGE > 0 ? 1 : 0) }">
        <c:choose>
            <c:when test="${i != page }">
                <a href="ItemsController?keyword=${keyword }&page=${i }">${i }</a>
            </c:when>
            <c:otherwise>${i }</c:otherwise>
        </c:choose>
    </c:forEach>
    <c:if test="${(page * ITEMS_OF_PAGE) <= numrows }">
        <a href="ItemsController?keyword=${keyword }&page=${page + 1 }">次 &gt;&gt;</a>
    </c:if>
</div>
</body>
</html>