<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>注文完了 - エム・ビー ショップ</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<h1>エム・ビー ショップ</h1>
<h2>注文完了</h2>
<p>下記の通り注文を受け付けました</p>
<table class="cart">
    <tr>
        <th>タイトル</th>
        <th>価格</th>
        <th>数量</th>
        <th>計</th>
    </tr>
    <c:set var="total" value="0"/>
    <c:forEach var="entry" items="${order.orderItems}">
    <tr>
        <td>${entry.item.title }</td>
        <td>${entry.price }円</td>
        <td>${entry.quantity }</td>
        <td>${entry.price * entry.quantity }円</td>
    </tr>
    <c:set var="total" value="${total + entry.price * entry.quantity }"/>
    </c:forEach>
    <tr>
        <th colspan="3">合計</th>
        <th>${total }円</th>
    </tr>
</table>
<h3>お届け先</h3>
<dl>
    <dt>名前</dt>
    <dd>${order.name }</dd>
    <dt>郵便番号</dt>
    <dd>${order.postal }</dd>
    <dt>住所</dt>
    <dd>${order.address }</dd>
    <dt>電話番号</dt>
    <dd>${order.phone }</dd>
</dl>
<p><a href="ItemsController">&lt;&lt; トップページへ</a></p>
</body>
</html>