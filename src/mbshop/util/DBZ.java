package mbshop.util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;

public class DBZ {
	public static Connection co = null;

	public static void connect() throws ClassNotFoundException, SQLException {
		ResourceBundle rb = ResourceBundle.getBundle("database");
		Class.forName(rb.getString("JDBC"));
		String url = rb.getString("driver") + "://" + rb.getString("dbhost") + ":" + rb.getString("dbport") + "/" + rb.getString("dbname");
		String user = rb.getString("dbuser");
		String pass = rb.getString("dbpass");
		co = DriverManager.getConnection(url, user, pass);
	}

	public static void close() throws ServletException {
		try {
			co.close();
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
