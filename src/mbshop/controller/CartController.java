package mbshop.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mbshop.model.Cart;
import mbshop.util.DBZ;

@WebServlet("/CartController")
public class CartController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			request.setCharacterEncoding("UTF-8");
			// データベース接続
			DBZ.connect();
			// カートオブジェクト取得
			HttpSession ses = request.getSession();
			Cart cart = (Cart)ses.getAttribute("cart");
			if (cart == null) {
				cart = new Cart();
			}
			// リクエストパラメータ初期化
			int mode = 0;
			int id = 0;
			
			// modeパラメータ取得
			String param = request.getParameter("mode");
			if (param != null) {
				mode = Integer.parseInt(param);
			}
			// modeによる分岐
			switch (mode) {
			case 1: // カートに入れる
				id = Integer.parseInt(request.getParameter("id"));
				cart.addItem(id);
				break;
			case 2: // 数量の更新
				id = Integer.parseInt(request.getParameter("id"));
				int quantity = Integer.parseInt(request.getParameter("quantity"));
				cart.updateQuantity(id, quantity);
				break;
			case 3: // カートから削除
				id = Integer.parseInt(request.getParameter("id"));
				cart.remove(id);
				break;
			}
			// スコープ属性のセット
			ses.setAttribute("cart", cart);
			request.setAttribute("cart", cart);
			// JSPへディスパッチ
			request.getRequestDispatcher("cart.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
