package mbshop.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mbshop.model.Cart;
import mbshop.model.Order;

@WebServlet("/OrdersController")
public class OrdersController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			
			// カートオブジェクトの取得
			HttpSession ses = request.getSession();
			Cart cart = (Cart)ses.getAttribute("cart");
			
			// リクエストパラメータの初期化
			int mode = 0;
			
			// modeパラメータの取得
			String param = request.getParameter("mode");
			if (param != null) {
				mode = Integer.parseInt(param);
			}
			// modeによる分岐
			switch (mode) {
			case 1: // 注文
				Order order = new Order(request.getParameterMap());
				order.setOrderItems(cart);
				order.save();
				
				// スコープにデータをセット
				request.setAttribute("order", order);
				ses.removeAttribute("cart");
				// JSPへディスパッチ
				request.getRequestDispatcher("ordered.jsp").forward(request, response);
				break;
			}
			
		} catch(Exception e) {
			throw new ServletException(e);
		}
	}

}
