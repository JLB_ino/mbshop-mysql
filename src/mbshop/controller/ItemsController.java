package mbshop.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mbshop.model.Item;
import mbshop.util.DBZ;

@WebServlet("/ItemsController")
public class ItemsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 商品一覧の1ページあたりの件数
	 */
	public static final int ITEMS_OF_PAGE = 5;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			// データベース接続
			DBZ.connect();
			// パラメータ初期値
			String keyword = "";
			int page = 1;
			int mode = 0;
			
			// modeパラメータ取得
			String param = request.getParameter("mode");
			if (param != null) {
				mode = Integer.parseInt(param);
			}
			// modeによる分岐
			switch (mode) {
			case 1: // 詳細画面処理
				// パラメータ取得
				int id = Integer.parseInt(request.getParameter("id"));
				// リクエストスコープ属性のセット
				request.setAttribute("item", Item.getItem(id));
				// JSPへディスパッチ
				request.getRequestDispatcher("detail.jsp").forward(request, response);
				break;
			default: // 商品一覧処理
				// パラメータ取得
				param = request.getParameter("keyword");
				if (param != null) {
					keyword = param;
				}
				param = request.getParameter("page");
				if (param != null) {
					page = Integer.parseInt(param);
				}
				
				// リクエストスコープ属性のセット
				request.setAttribute("ITEMS_OF_PAGE", ITEMS_OF_PAGE);
				request.setAttribute("keyword", keyword);
				request.setAttribute("page", page);
				request.setAttribute("numrows", Item.getNumRows(keyword));
				request.setAttribute("list", Item.getList(keyword, page));
				// JSPへディスパッチ
				request.getRequestDispatcher("list.jsp").forward(request, response);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
	}

}
