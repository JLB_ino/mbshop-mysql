package mbshop.model;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Cart extends LinkedHashMap<Integer, Map<String, Object>> {

	private static final long serialVersionUID = 1L;

	/**
	 * 指定された商品をカート情報に追加する
	 * @param id 商品ID
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void addItem(int id) throws SQLException, ParseException {
		Map<String, Object> cartItem = get(id);
		if (cartItem == null) {
			cartItem = new HashMap<String, Object>();
			cartItem.put("item", Item.getItem(id));
			cartItem.put("quantity", 1);
			put(id, cartItem);
		} else {
			cartItem.put("quantity", (Integer)cartItem.get("quantity") + 1);
		}
	}
	
	/**
	 * 指定された商品の数量を更新する
	 * @param id 商品ID
	 * @param quantity 数量
	 */
	public void updateQuantity(int id, int quantity) {
		Map<String, Object> cartItem = get(id);
		cartItem.put("quantity", quantity);
	}
}
