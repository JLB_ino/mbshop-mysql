package mbshop.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mbshop.controller.ItemsController;
import mbshop.util.DBZ;

public class Item {
	public static final int ITEMS_OF_PAGE = ItemsController.ITEMS_OF_PAGE;
	
	private int id;
	private String title;
	private int price;
	private String players;
	private String directors;
	private String description;
	private Timestamp updated;
	private Timestamp created;
	
	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id セットする id
	 */
	public void setId(String id) {
		this.id = Integer.parseInt(id);
	}
	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title セットする title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return price
	 */
	public int getPrice() {
		return price;
	}
	/**
	 * @param price セットする price
	 */
	public void setPrice(String price) {
		this.price = Integer.parseInt(price);
	}
	/**
	 * @return players
	 */
	public String getPlayers() {
		return players;
	}
	/**
	 * @param players セットする players
	 */
	public void setPlayers(String players) {
		this.players = players;
	}
	/**
	 * @return directors
	 */
	public String getDirectors() {
		return directors;
	}
	/**
	 * @param directors セットする directors
	 */
	public void setDirectors(String directors) {
		this.directors = directors;
	}
	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description セットする description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return updated
	 */
	public Timestamp getUpdated() {
		return updated;
	}
	/**
	 * @param updated セットする updated
	 * @throws ParseException 
	 */
	public void setUpdated(String updated) throws ParseException {
		Timestamp time = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(updated).getTime());
		this.updated = time;
	}
	/**
	 * @return created
	 */
	public Timestamp getCreated() {
		return created;
	}
	/**
	 * @param created セットする created
	 * @throws ParseException 
	 */
	public void setCreated(String created) throws ParseException {
		Timestamp time = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(created).getTime());
		this.created = time;
	}
	
	/**
	 * デフォルトコンストラクタ
	 */
	public Item() {}
	/**
	 * リクエストパラメータからオブジェクトを生成する
	 * @param params リクエストパラメータ
	 */
	public Item(Map<String, String[]> params) {
		String[] ids = params.get("id");
		if (ids != null) {
			setId(ids[0]);
		}
		setTitle(params.get("title")[0]);
		setPrice(params.get("price")[0]);
		setPlayers(params.get("players")[0]);
		setDirectors(params.get("directors")[0]);
		setDescription(params.get("description")[0]);
	}
	
	/**
	 * キーワードによりタイトルを検索し、オブジェクトを生成してList化する
	 * @param keyword 検索キーワード
	 * @param page 表示するページ番号
	 * @return List<Item>オブジェクト
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static List<Item> getList(String keyword, int page) throws SQLException, ParseException {
		List<Item> list = new LinkedList<Item>();
		
		String sql = "SELECT * FROM items WHERE title LIKE ? ORDER BY id LIMIT " + ITEMS_OF_PAGE + " OFFSET "  + (page - 1) * ITEMS_OF_PAGE;
		PreparedStatement ps = DBZ.co.prepareStatement(sql);
		ps.setString(1, "%" + keyword + "%");
		ResultSet rs = ps.executeQuery();
		int counter = 0;
		while (rs.next()) {
			Item i = new Item();
			i.setId(rs.getString("id"));
			if (counter == 0 || counter % 10 == 0) {
				i.setTitle("★当店のおすすめ★：" + rs.getString("title"));
			} else {
				i.setTitle(rs.getString("title"));
			}
			i.setPrice(rs.getString("price"));
			i.setPlayers(rs.getString("players"));
			i.setDirectors(rs.getString("directors"));
			i.setDescription(rs.getString("description"));
			i.setUpdated(rs.getString("updated"));
			i.setCreated(rs.getString("created"));
			list.add(i);
			
			counter++;
		}
		
		return list;
	}
	public static List<Item> getList(String keyword) throws SQLException, ParseException {
		return getList(keyword, 1);
	}
	
	/**
	 * 商品IDを指定してオブジェクトを取得する
	 * @param id 商品ID
	 * @return Itemオブジェクト
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static Item getItem(int id) throws SQLException, ParseException {
		String sql = "SELECT * FROM items WHERE id = ?";
		PreparedStatement ps = DBZ.co.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		rs.next();
		Item i = new Item();
		i.setId(rs.getString("id"));
		i.setTitle(rs.getString("title"));
		i.setPrice(rs.getString("price"));
		i.setPlayers(rs.getString("players"));
		i.setDirectors(rs.getString("directors"));
		i.setDescription(rs.getString("description"));
		i.setUpdated(rs.getString("updated"));
		i.setCreated(rs.getString("created"));
		return i;
	}
	
	/**
	 * タイトルにkeywordを含む行の総数を取得する
	 * @param keyword 検索キーワード
	 * @return 行数
	 * @throws SQLException
	 */
	public static int getNumRows(String keyword) throws SQLException {
		String sql = "SELECT COUNT(*) FROM items WHERE title LIKE ?";
		PreparedStatement ps = DBZ.co.prepareStatement(sql);
		ps.setString(1, "%" + keyword + "%");
		ResultSet rs = ps.executeQuery();
		rs.next();
		return rs.getInt(1);
	}
}
