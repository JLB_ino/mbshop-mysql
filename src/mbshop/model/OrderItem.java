package mbshop.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Map;

import mbshop.util.DBZ;

public class OrderItem {
	private int id;
	private int orderId;
	private int itemId;
	private int price;
	private int quantity;
	private Timestamp updated;
	private Timestamp created;
	private Item item;
	
	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return orderId
	 */
	public int getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId セットする orderId
	 */
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return itemId
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * @param itemId セットする itemId
	 */
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price セットする price
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity セットする quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return updated
	 */
	public Timestamp getUpdated() {
		return updated;
	}

	/**
	 * @param updated セットする updated
	 */
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	/**
	 * @return created
	 */
	public Timestamp getCreated() {
		return created;
	}

	/**
	 * @param created セットする created
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}

	/**
	 * @return item
	 */
	public Item getItem() {
		return item;
	}

	/**
	 * @throws ParseException 
	 * @throws SQLException 
	 * 
	 */
	public void setItem() throws SQLException, ParseException {
		this.item = Item.getItem(itemId);
	}

	/**
	 * デフォルトコンストラクタ
	 */
	OrderItem() {}
	/**
	 * カートのエントリー1件の情報からインスタンスを生成する
	 * @param cartEntry カートエントリー情報（1件）
	 * @throws SQLException
	 * @throws ParseException
	 */
	OrderItem(Map<String, Object> cartEntry) throws SQLException, ParseException {
		Item item = (Item)cartEntry.get("item");
		int quantity = (Integer)cartEntry.get("quantity");
		setItemId(item.getId());
		setPrice(item.getPrice());
		setQuantity(quantity);
		setItem();
	}
	
	/**
	 * 注文IDを指定してデータをデータベースに保存する
	 * @param orderId 注文ID
	 * @throws SQLException
	 */
	public void save(int orderId) throws SQLException {
		setOrderId(orderId);
		save();
	}
	
	/**
	 * データをデータベースに保存する
	 * @throws SQLException
	 */
	public void save() throws SQLException {
		String sql;
		if (id == 0) {
			sql = "INSERT INTO order_items(order_id, item_id, price, quantity, updated, created) VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
			PreparedStatement ps = DBZ.co.prepareStatement(sql);
			ps.setInt(1, orderId);
			ps.setInt(2, itemId);
			ps.setInt(3, price);
			ps.setInt(4, quantity);
			ps.executeUpdate();
		}
	}
}
