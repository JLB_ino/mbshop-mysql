package mbshop.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mbshop.util.DBZ;

public class Order {
	private int id;
	private String name;
	private String postal;
	private String address;
	private String phone;
	private Timestamp updated;
	private Timestamp created;
	private List<OrderItem> orderItems = new LinkedList<OrderItem>();
	
	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id セットする id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param id セットする id
	 */
	public void setId(String id) {
		this.id = Integer.parseInt(id);
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return postal
	 */
	public String getPostal() {
		return postal;
	}
	/**
	 * @param postal セットする postal
	 */
	public void setPostal(String postal) {
		this.postal = postal;
	}
	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address セットする address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone セットする phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return updated
	 */
	public Timestamp getUpdated() {
		return updated;
	}
	/**
	 * @param updated セットする updated
	 */
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	/**
	 * @return created
	 */
	public Timestamp getCreated() {
		return created;
	}
	/**
	 * @param created セットする created
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	/**
	 * @return orderItems
	 */
	public List<OrderItem> getOrderItems() {
		return orderItems;
	}
	/**
	 * Cartインスタンスから明細情報（OrderItemインスタンス）を生成する
	 * @param cart Cartインスタンス
	 * @throws ParseException 
	 * @throws SQLException 
	 */
	public void setOrderItems(Cart cart) throws SQLException, ParseException {
		for (Integer key: cart.keySet()) {
			this.orderItems.add(new OrderItem(cart.get(key)));
		}
	}
	
	/**
	 * デフォルトコンストラクタ
	 */
	public Order(){}
	/**
	 * リクエストパラメータからインスタンスを生成する
	 * @param params リクエストパラメータ
	 */
	public Order(Map<String, String[]> params) {
		String[] ids = params.get("id");
		if (ids != null) {
			setId(ids[0]);
		}
		setName(params.get("name")[0]);
		setPostal(params.get("postal")[0]);
		setAddress(params.get("address")[0]);
		setPhone(params.get("phone")[0]);
	}
	
	/**
	 * データをデータベースに保存する
	 * @return 注文ID
	 * @throws SQLException
	 */
	public int save() throws SQLException {
		String sql;
		if (id == 0) {
			// 新規登録
			sql = "INSERT INTO orders(name, postal, address, phone, updated, created) VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
			PreparedStatement ps = DBZ.co.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, postal);
			ps.setString(3, address);
			ps.setString(4, phone);
			ps.executeUpdate();
			
			// 新しい注文IDを取得する
			sql = "SELECT LAST_INSERT_ID()";
			ps = DBZ.co.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			rs.next();
			setId(rs.getInt(1));
			
			// 注文明細データを保存する
			for (OrderItem item: orderItems) {
				item.save(id);
			}
		} else {
			sql = "";
		}
		
		return id;
	}
}
