package mbshop.test;

import java.util.HashMap;
import java.util.Map;

import mbshop.model.Item;

public class ItemTest {

	public static void main(String[] args) {

		System.out.println("UT001:" + UT001());
	}
	
	public static boolean UT001() {
		
		// このテストはUT001
		Map<String, String[]> params = new HashMap<>();
		
		params.put("id", new String[]{ "1" });
		
		Item item = new Item(params);
		
		boolean b = true;
		
		if (item.getId() != 1) {
			b = false;
		}
		
		if ("テッド".equals(item.getTitle())) {
			b = false;
		}
		
		return b;
	}
}